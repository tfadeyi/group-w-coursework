0. Clone this repository with ssh or https
1. Install python if not present in your computer (I'm using python 2.7)
2. install python package manager `pip`
3. Install virtualenv via pip: ```pip install virtualenv==16.1.0```
   Test your installation: ```virtualenv --version```
4. Change directory: ```cd backend```
5. Create virtual environment: ```virtualenv cooking_app_api```
   virtualenv cooking_app_api will create a folder in the backend directory which will contain the Python executable files, and a copy of the pip library which you can use to install other packages.
The name of the virtual environment (in this case, it was cooking_app_api) can be anything; omitting the name will place the files in the current directory instead.
6. To begin using the virtual environment, it needs to be activated:```source cooking_app_api/bin/activate```
   NOTE(don't run it now!): If you are done working in the virtual environment for the moment, you can deactivate it:```deactivate```
7. (For linux) Run ```sudo apt-get install default-libmysqlclient-dev```
8. Run ```pip install -r requirements.txt```
9. Change directory: ```cd ../frontend```
10. Run ```npm install``` (migth take a while)
   (this for frontend development) ```npm start```
11. Run ```npm run build```
12. Run ```python ../backend/project_api/manage.py collectstatic```
13. Run ```cd ../backend/project_api```
14. (this for GAE deployment) Run ```pip install -t lib -r requirements.txt```
15. Make sure you have your mysql instance running.
16. Must set database dsn, use ```export COOKING_APP_DATABASE_DSN="mysql://root:cookingbook@tcp(0.0.0.0:3306)/cookingbook"```
    we are using an environment variable.
17. To run the lightweight server for development on http://127.0.0.1:8000/
use: ```python manage.py runserver```
18. If you have gcloud and your project setup you can just run: ```cd ../``` to go back to the backend directory + ```gcloud app deploy```


Database:

0.  Depending on your preference you can either download and install MySQL directly
    or using Docker.
    -If you don't want to use Docker https://dev.mysql.com/doc/mysql-getting-started/en/#mysql-getting-started-installing
      (Remember to to set the database name and password to ```cookingbook```)
    -If you already have Docker installed just use this command ```docker run -d -p 3306:3306 --name cookingbook-database-mysql -e MYSQL_DATABASE="cookingbook" -e MYSQL_ROOT_PASSWORD=cookingbook mysql:5.7```
      (to connect to the database locally with docker just use this command: ```docker run -it --link cookingbook-database-mysql:mysql --rm mysql sh -c 'exec mysql -h"$MYSQL_PORT_3306_TCP_ADDR" -P"$MYSQL_PORT_3306_TCP_PORT" -uroot -p"$MYSQL_ENV_MYSQL_ROOT_PASSWORD"'``` , this will open the mysql shell)
1. Then do ```cd backend```
2.  Run ```source cooking_app_api/bin/activate```
3.  Run ```pip install -r requirements.txt``` (only the first time)
4. If everything is working correctly then just set the database DSN to route to your local database,
    use this command ```export COOKING_APP_DATABASE_DSN="mysql://root:cookingbook@tcp(0.0.0.0:3306)/cookingbook"```
    we are using an environment variable.
5.  Then run ```python manage.py migrate``` to update the database, with the latest changes
6. Run the database ```python manage.py runserver```
