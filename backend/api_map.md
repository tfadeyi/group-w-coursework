Register users: api/users/ use http post
Login user: api/auth/login (need username,email and password)
Verify JWT: api/auth/token/ (POST request, with json payload only field is token)
Get user: api/users/{id} use http.get
Delete user: api/users/{id} use http.delete (only available to admins)
Get user list: api/users/ (only available to admins)

Add recipe: api/recipes/ use http post
Get recipe: api/recipes/{id} use http get
Get recipe full list: api/recipes/ use http get
Get recipe filtered list by name or cuisine: api/recipes/?search=international use http get
