virtualenv is a tool to create isolated Python environments. virtualenv creates a folder which contains all the necessary executables to use the packages that a Python project would need.

It can be used standalone, in place of Pipenv.

Install virtualenv via pip:

```pip install virtualenv==16.1.0```

My version is 16.1.0

Test your installation

```virtualenv --version```

Create a virtual environment for a project:

```cd backend```

```virtualenv cooking_app_api```
you can change the name to whatever you want.

virtualenv cooking_app_api will create a folder in the backend directory which will contain the Python executable files, and a copy of the pip library which you can use to install other packages.
The name of the virtual environment (in this case, it was cooking_app_api) can be anything; omitting the name will place the files in the current directory instead.

This creates a copy of Python in whichever directory you ran the command in, placing it in a folder named cooking_app_api.

You can also use the Python interpreter of your choice (like python2.7).

```virtualenv -p /usr/bin/python2.7 my_project```

To begin using the virtual environment, it needs to be activated:

```source my_project/bin/activate```

The name of the current virtual environment will now appear on the left of the prompt (e.g. (cooking_app_api)Your-Computer:your_project UserName$) to let you know that it’s active. From now on, any package that you install using pip will be placed in the my_project folder, isolated from the global Python installation.

Install packages as usual, for example:

```pip install requests```

If you are done working in the virtual environment for the moment, you can deactivate it:

```deactivate```

This puts you back to the system’s default Python interpreter with all its installed libraries.

requirements.txt, shows the list of dependencies required by the virtual environment.
To have similar dev environments run ```pip install -r requirements.txt``` inside your virtualenv

More info: https://docs.python-guide.org/dev/virtualenvs/


To install Django use:

```pip install Django==1.11.16```

the version that I'm working on is 1.11.16 

Use ```python -m django --version``` to check the version

To run the lightweight server for development on http://127.0.0.1:8000/
use ```python manage.py runserver``` inside the project_api folder
