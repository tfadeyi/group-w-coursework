# -*- coding: utf-8 -*-
# Generated by Django 1.11.16 on 2018-12-27 13:49
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_auto_20181226_1554'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='name',
        ),
        migrations.AddField(
            model_name='user',
            name='password',
            field=models.CharField(default='Password1', max_length=30),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='user',
            name='username',
            field=models.CharField(default='user1', max_length=50),
            preserve_default=False,
        ),
    ]
