# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse

from rest_framework import generics
from .serializers import UserSerializer, RecipeSerializer
from .models import User, Recipe
from rest_framework import viewsets

from rest_framework.permissions import AllowAny
from api.permissions import IsLoggedInUserOrAdmin, IsAdminUser
from rest_framework import filters

class RecipeViewSet(viewsets.ModelViewSet):
    #This class handles the http POST/DELETE/PUT/GET requests.
    #filtered search, /api/recipes/?search=international
    queryset = Recipe.objects.all()
    serializer_class = RecipeSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('$name','cuisine_type')

class UserViewSet(viewsets.ModelViewSet):
    #This class handles the all http requests POST/DELETE/PUT/GET.
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_permissions(self):
        permission_classes = []
        if self.action == 'create':
            permission_classes = [AllowAny]
        elif self.action == 'retrieve' or self.action == 'update' or self.action == 'partial_update':
            permission_classes = [IsLoggedInUserOrAdmin]
        elif self.action == 'list' or self.action == 'destroy':
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]
