from django.conf.urls import url, include
from .views import RecipeViewSet, UserViewSet
from rest_framework import routers
from rest_framework_jwt.views import verify_jwt_token

router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'recipes', RecipeViewSet)

urlpatterns = [
    url(r'^auth/', include('rest_auth.urls')),
    url(r'^auth/token/', verify_jwt_token),
    url(r'^', include(router.urls)),
    url(r'^test/users/(?P<pk>[0-9]+)/$',
        UserViewSet.as_view({
            'get': 'retrieve',
            'delete': 'destroy'
        }),
        name='user_details'),
    url(r'^test/recipes/$',
        RecipeViewSet.as_view({
            'post': 'create'
        }),
        name='recipe_create'),
    url(r'^test/recipes/(?P<pk>[0-9]+)/$',
        RecipeViewSet.as_view({
            'get': 'retrieve',
            'delete': 'destroy'
        }),
        name='recipe_details'),
]
