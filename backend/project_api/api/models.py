# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

# both schemas should be expanded

# User model(Table name: api_user)


class User(AbstractUser):
    username = models.CharField(blank=True, null=True, max_length=50)
    email = models.EmailField(_('email address'), unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']


# Recipe model(Table name: api_recipe)
class Recipe(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=1500)
    creator_id = models.ForeignKey(User, on_delete=models.CASCADE)
    cuisine_type = models.CharField(max_length=200, default="not defined")
    steps = models.CharField(max_length=1500, default="not defined")
