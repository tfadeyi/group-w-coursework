from rest_framework import serializers
from .models import User, Recipe
from django.utils.html import strip_tags


# maps models into JSON
class RecipeSerializer(serializers.ModelSerializer):

    # Meta class to map serializer's fields with the model fields.
    class Meta:
        model = Recipe
        fields = ('id', 'name', 'description',
                  'creator_id', 'cuisine_type', 'steps')

    def create(self, validated_data):
        tmp_name = validated_data.pop('name')
        name = strip_tags(tmp_name)
        validated_data['name'] = name
        recipe = Recipe(**validated_data)
        recipe.save()
        return recipe


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'email', 'username', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        password = validated_data.pop('password')
        user = User(**validated_data)
        user.set_password(password)
        user.save()
        return user
