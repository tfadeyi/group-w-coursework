# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from rest_framework.test import APIClient
from rest_framework import status
from django.urls import reverse
from django.test import TestCase
from .models import Recipe, User
from django.conf.urls import url


class ModelTestCase(TestCase):
    def setUp(self):
        self.user = User(
            username="u1", email="test@gmail.com", password="PassW0rd!")

    def test_model_create_user(self):
        old_count = User.objects.count()
        self.user.save()
        new_count = User.objects.count()
        self.assertNotEqual(old_count, new_count)
        self.user.delete()

    def test_model_create_recipe(self):
        self.user.save()
        recipe = Recipe(
            name="Recipe#1",
            description="Brand new recipe!",
            creator_id=self.user)
        old_count = Recipe.objects.count()
        recipe.save()
        new_count = Recipe.objects.count()
        self.assertNotEqual(old_count, new_count)
        recipe.delete()
        self.user.delete()


class ViewTestCase(TestCase):
    def setUp(self):
        self.user = User(
            username="u1", email="test@gmail.com", password="PassW0rd!")
        self.user.is_staff = True
        self.user.save()
        self.client = APIClient()
        self.client.force_authenticate(user=self.user)

        self.recipe_data = {
            'name': 'Recipe#2',
            'description': 'Second recipe',
            'creator_id': self.user.id
        }
        self.response = self.client.post(
            reverse("recipe_create"), self.recipe_data, format="json")

    #RECIPE tests
    def test_api_can_create_a_recipe(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_api_can_get_a_recipe(self):
        #"""Test the api can get a given recipe."""
        recipe = Recipe.objects.get()
        response = self.client.get(
            reverse('recipe_details', kwargs={'pk': recipe.id}), format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, recipe.name)

    #def test_api_can_update_recipe(self):
    #"""Test the api can update a given recipe."""
    #    recipe = Recipe.objects.get()
    #    changed_recipe = {'name':'NewRecipe'}
    #    res = self.client.put(
    #      reverse('recipe_details', kwargs={'pk': recipe.id}),
    #      changed_recipe, format='json'
    #    )
    #    self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_api_can_delete_recipe(self):
        #"""Test the api can delete a recipe."""
        recipe = Recipe.objects.get()
        response = self.client.delete(
            reverse('recipe_details', kwargs={'pk': recipe.id}),
            format='json',
            follow=True)

        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)

    #USER tests
    def test_api_can_get_a_user(self):
        #Test the api can get a given user.
        user = User.objects.get()
        response = self.client.get(
            reverse('user_details', kwargs={'pk': user.id}), format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, user.username)

    def test_api_can_delete_user(self):
        #Test the api can delete a user.
        response = self.client.delete(
            reverse('user_details', kwargs={'pk': self.user.id}),
            format='json',
            follow=True)

        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)
