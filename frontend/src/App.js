import React, { Component } from 'react';
import { Navbar, Nav, NavItem, FormGroup, FormControl, Button, Form } from "react-bootstrap";
import { Link, withRouter } from "react-router-dom"; //withRouter was removed, to pass some test, it wasn't used (add it again if needed)
import { LinkContainer } from 'react-router-bootstrap';
import './App.css';
import Routes from "./Route";

class App extends Component {
  constructor(props) {
      super(props);
      this.state = {
        isAuthenticated: false,
        isAuthenticating: true,
    };
    }

  async componentDidMount() {
    try {

      // check the localstorage 
      if (localStorage.token) {
        this.userHasAuthenticated(true);
      }
    }
    catch (e) {
      if (e !== 'No current user') {
        alert(e);
      }
    }
  }

  handleChange = async event => {
        await this.setState({
            [event.target.id]: event.target.value
      });
      sessionStorage['keywords'] = this.state.search
      console.log(this.state.search)
      console.log(sessionStorage['keywords'])
    }

  userHasAuthenticated = authenticated => {
    this.setState({ isAuthenticated: authenticated })
    console.log(authenticated)
  }

  handleLogout = async event => {
    // clearing the localstorage and sessionstorage to log out 
    await localStorage.clear()
    await sessionStorage.clear()
    this.userHasAuthenticated(false)
    console.log(localStorage)
    console.log(sessionStorage)
    console.log(this.state.isAuthenticated)
    this.props.history.push("/")
  }

    handleSubmitSearch = async event => {
        this.props.history.push(`/advsearch/${this.state.search}`)
    }
    

  renderNavbar() {
    return (
      <Navbar inverse collapseOnSelect>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to="/">Cooking Cooking</Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Navbar.Form pullLeft>
            <FormGroup>
              <FormControl type="text" placeholder="Search for recipes" />
                    </FormGroup>{' '}

                    <Button bsStyle='success'>Search</Button>

                </Navbar.Form>
          <Nav pullRight>
            <LinkContainer to='/Signup'>
              <NavItem eventKey={1} >
                Signup
            </NavItem>
            </LinkContainer>
            <LinkContainer to='/login'>
              <NavItem eventKey={2} >
                Login
              </NavItem>
            </LinkContainer>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
  renderUserNavbar() {
    return (
      <Navbar inverse collapseOnSelect>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to="/">Cooking Cooking</Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
                <Navbar.Form pullLeft>
                    <Form onSubmit={this.handleSubmitSearch}>
                    <FormGroup controlId='search'>
                        <FormControl type="text" placeholder="Search for recipes" onChange={this.handleChange}/>
                    </FormGroup>{' '}

                    <Button type="submit" bsStyle='success'>Search</Button>
                    </Form>
          </Navbar.Form>
          <Nav pullRight>
            <NavItem eventKey={2} onClick={this.handleLogout}>
              Logout
            </NavItem>
                    <LinkContainer to='/user'>
                        <NavItem eventKey={3}>
                            User
            </NavItem>
                        </LinkContainer>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
  }
  render() {
    const childProps = {
      isAuthenticated: this.state.isAuthenticated,
      userHasAuthenticated: this.userHasAuthenticated,

      username: this.state.username,  //this is going to show in the navbar.
      setUserName: this.setUserName,

    };
    return (
      <div className="App">
        {this.state.isAuthenticated === true
          ? this.renderUserNavbar()
          : this.renderNavbar()}
        <Routes childProps={childProps} />
      </div>
    );
  }
}

export default withRouter(App);
