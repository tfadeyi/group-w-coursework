import React, { Component } from "react";
import { Row, Col, Nav, NavItem, Tab, Grid, Thumbnail, Well, Button } from "react-bootstrap";
import { LinkContainer } from 'react-router-bootstrap';
import "./UserHome.css";

export default class UserHome extends Component {
  constructor(props) {
    super(props);
  };
  render() {
    return (
      <div className="UserHome">
        <Tab.Container id="left-tabs" defaultActiveKey="first">
          <Row className="clearfix">
            <Col md={3}>
              <Nav bsStyle="pills" stacked>
                <NavItem eventKey="first">All Recipes</NavItem>
                <NavItem eventKey="second">Your Recipes</NavItem>
                <NavItem eventKey="third">Your Profile</NavItem>
              </Nav>
            </Col>
            <Col md={9}>
              <Tab.Content animation>
                <Tab.Pane eventKey="first">
                  <Grid>
                    <Row>
                      <Col xs={6} md={3}>
                        <Thumbnail src={require("../temp_pic/food1.jpg")} />
                        <h3>Recipe Name</h3>
                        <p>Description</p>
                        <p>
                          <LinkContainer to="/user/recipeId">
                            <Button bsStyle="primary">See Detail</Button>
                          </LinkContainer>
                        </p>
                      </Col>
                      <Col xs={6} md={3}>
                        <Thumbnail src={require("../temp_pic/food2.jpg")} />
                        <h3>Recipe Name</h3>
                        <p>Description</p>
                        <p>
                          <Button bsStyle="primary">See Detail</Button>
                        </p>
                      </Col>
                      <Col xs={6} md={3}>
                        <Thumbnail alt="171x180" src="/thumbnail.png" />
                      </Col>
                    </Row><br />
                    <Row>
                      <Col xs={6} md={3}>
                        <Thumbnail alt="171x180" src="/thumbnail.png" />
                      </Col>
                      <Col xs={6} md={3}>
                        <Thumbnail alt="171x180" src="/thumbnail.png" />
                      </Col>
                      <Col xs={6} md={3}>
                        <Thumbnail alt="171x180" src="/thumbnail.png" />
                      </Col>
                    </Row>
                  </Grid>
                </Tab.Pane>

                <Tab.Pane eventKey="second">
                  <Grid>
                    <Row>
                      <Col xs={6} md={3}>
                        <Thumbnail src={require("../temp_pic/add.png")} />
                        <LinkContainer to="/user/create">
                          <Button bsStyle="primary" bsSize='lg'>Add New Recipe</Button>
                        </LinkContainer>
                      </Col>
                      <Col xs={6} md={3}>
                        <Thumbnail src={require("../temp_pic/food4.jpeg")} />
                        <h3>Recipe Name</h3>
                        <p>Description</p>
                        <p>
                          <Button bsStyle="primary">Set Detail</Button>
                        </p>
                      </Col>
                      <Col xs={6} md={3}>
                        <Thumbnail src={require("../temp_pic/food1.jpg")} />
                        <h3>Recipe Name</h3>
                        <p>Description</p>
                        <p>
                          <Button bsStyle="primary">Set Detail</Button>
                        </p>
                      </Col>
                    </Row>
                  </Grid>
                </Tab.Pane>

                <Tab.Pane eventKey="third">
                  <h2>Username:</h2><br />
                  <Well bsSize="small">{localStorage.username}</Well>
                  <hr className='line' />
                  <h2>Email:</h2><br />
                  <Well bsSize="small">{localStorage.email}</Well>
                  <hr className='line' />
                  <h2>Password:</h2><br />
                  <Button bsStyle='primary'>Change Password</Button>
                </Tab.Pane>
              </Tab.Content>
            </Col>
          </Row>
        </Tab.Container>
      </div>
    )
  }
}