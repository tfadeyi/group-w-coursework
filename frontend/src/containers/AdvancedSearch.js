import React, { Component } from "react";
import { Button, Col, Row} from "react-bootstrap";
import { LinkContainer } from 'react-router-bootstrap';
import "./AdvancedSearch.css";
import axios from "axios";

export default class AdvancedSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ingredients: [{ name: '' }],
            recipes: [],
            search: '',
        }
    }
    async componentWillMount () {
        console.log(111)
        console.log(sessionStorage.keywords)
        try {
             await axios({
                method: 'GET',
                url: `http://127.0.0.1:8000/api/recipes?search=${sessionStorage.keywords}`,
                headers: {
                    'authorization': "JWT " + localStorage.token,
                }
            })
                .then(data => (
                    this.setState({
                        recipes: data.data,
                    })
                ))
        }
        catch (e) {
            console.log(e)
        }
        // console.log(this.state.info[0].id)
    }

    //async componentDidMount() {
    //    console.log(111)
    //    console.log(sessionStorage.keywords)
    //    try {
    //        await axios({
    //            method: 'GET',
    //            url: `http://127.0.0.1:8000/api/recipes?search=${sessionStorage.keywords}`,
    //            headers: {
    //                'authorization': "JWT " + localStorage.token,
    //            }
    //        })
    //            .then(data => (
    //                this.setState({
    //                    recipes: data.data,
    //                })
    //            ))
    //    }
    //    catch (e) {
    //        console.log(e)
    //    }
    //    // console.log(this.state.info[0].id)
    //}


    handleShowRecipes = () => {
        let content = []
        let row = []
        this.state.recipes.forEach((recipe, i) => {
            if ((i) % 3 === 0) {
                row = []
                row.push(
                    <Col xs={6} md={4} key={i}>
                        {/* <Thumbnail src={require("../temp_pic/"+recipe.photo.trim())} /> */}
                        <h3>{recipe.name}</h3>
                        <p>{recipe.cuisine_type}</p >
                        <LinkContainer to={`/user/recipe/${recipe.id}`}>
                            <Button bsStyle="primary">See Detail</Button>
                        </LinkContainer>
                    </Col>
                )
            }
            else {
                row.push(
                    <Col xs={6} md={4} key={i}>
                        {/* <Thumbnail src={require("../temp_pic/"+recipe.photo.trim())} /> */}
                        <h3>{recipe.name}</h3>
                        <p>{recipe.cuisine_type}</p >
                        <LinkContainer to={`/user/recipe/${recipe.id}`}>
                            <Button bsStyle="primary">See Detail</Button>
                        </LinkContainer>
                    </Col>
                )
            }
            if (i % 3 === 2 || (i + 1) === this.state.recipes.length) {
                content.push(<Row key={i}>{row}</Row>)
            }
        })
        return (content)
    }

    render() {
        return (
            <div className='Search'>
                {this.handleShowRecipes()}
            </div>
        )
    }


}