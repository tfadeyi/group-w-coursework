import React, { Component } from "react";
import { Thumbnail, Row, Col, Table, Well, Panel } from "react-bootstrap";
import "./ShowDetails.css";


export default class ShowDetails extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: 'sb',
      cuisine: 'sss',
      description: 'aaaa: 4, bbb: 2, cccc: 5',
      steps: 'jlskdfjklskdlflss. //lskdlfwefklwlfsd. //sdlfkskldf.',
      photos: "food1.jpg, food2.jpg, food2.jpeg, food3.jpg"
    }
  }

  handlePhotos = () => {
    let content = []
    let row = []
    this.state.photos.split(',').forEach((photo, i) => {
      if ((i) % 3 === 0) {
        row = []
        row.push(
          <Col xs={6} md={4} key={i}>
            <Thumbnail src={require("../temp_pic/" + photo.trim())} />
            {console.log(photo)}
          </Col>
        )
      }
      else {
        row.push(
          <Col xs={6} md={4} key={i}>
            <Thumbnail src={require("../temp_pic/" + photo.trim())} />
          </Col>
        )
      }
      if (i % 3 === 2 || (i + 1) === this.state.photos.split(',').length) {
        content.push(<Row key={i}>{row}</Row>)
      }
    })
    return (content)
  }


  render() {
    return (

      <div className='Details'>

        <h2>Name of The Recipe</h2>
        <Well>{this.state.name}</Well>
        <br />

        <h2>Cuisine</h2>
        <Well>{this.state.cuisine}</Well>
        <br />

        <h2>Ingredients</h2><br />
        <div className='Table'>
          <Table striped bordered condensed hover>
            <thead>
              <tr>
                <th>Ingredient</th>
                <th>Quantity</th>
              </tr>
            </thead>
            <tbody>

              {this.state.description.split(',').map((ingredient, i) => (
                <tr key={i}>
                  <td>{ingredient.split(':')[0]}</td>
                  <td>{ingredient.split(':')[1]}</td>
                </tr>
              ))}
            </tbody>


          </Table>
        </div><br />

        <h2>Steps</h2><br />
        <div className='Steps'>

          {this.state.steps.split('//').map((step, i) => (
            <Panel key={i}>
              <Panel.Heading>Step {i + 1}</Panel.Heading>
              <Panel.Body>{step}</Panel.Body>
            </Panel>
          ))
          }
        </div><br />
        <h2>Photos</h2>

        <div className='Photos'>
          {this.handlePhotos()}
        </div>


      </div>
    )
  }
}