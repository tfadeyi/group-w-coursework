import React, { Component } from "react";
import { Carousel, } from "react-bootstrap";
import "./Home.css";

export default class Home extends Component {
  render() {
    // console.log(localStorage)
    // test whether the token and username have successfully stored into local storage
    return (
      <div className="Home">
        <Carousel>
          <Carousel.Item>
            <img className='image' alt="food1" src={require("../temp_pic/food1.jpg")} />
            <Carousel.Caption>
              <h2>Reconmended Recipe</h2>
              <p>XXXXXXXXXXXXXXXX</p>
            </Carousel.Caption>
          </Carousel.Item>
          <Carousel.Item>
            <img className='image' alt="food2" src={require("../temp_pic/food2.jpeg")} />
            <Carousel.Caption>
              <h2>Reconmended Recipe</h2>
              <p>XXXXXXXXXXXXXXXX</p>
            </Carousel.Caption>
          </Carousel.Item>

        </Carousel>
      </div>
    );
  }
}