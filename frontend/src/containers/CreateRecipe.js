import React, { Component } from "react";

import { FormControl, Form, FormGroup, Button, InputGroup, DropdownButton, MenuItem } from "react-bootstrap";
//import {ImageUploader} from "react-images-upload"; //was removed, to pass some test, it wasn't used (add it again if needed)
import "./CreateRecipe.css";
import axios from 'axios'

export default class CreateRecipe extends Component {

  constructor(props) {
    super(props);
    this.state = {
      recipes: [{ name: '', quantity: '' }],
      steps: [{ description: '' }],
      pictures: [],
      cuisines: ['Not Specified', 'Chinese', 'Thai', 'Spanish', 'Italian'],
      cuisineSelect: 'Not Specified'
    };
    this.uploadImage = this.uploadImage.bind(this);
  }

  handleAddRecipe = () => {
    this.setState({
      recipes: this.state.recipes.concat([{ name: '', quantity: '' }])
    });
  }

  handleRemoveRecipe = (i) => () => {
    this.setState({
      recipes: this.state.recipes.filter((s, si) => i !== si)
    });
  }

  handleRecipeChange = (i) => (evt) => {
    const newRecipes = this.state.recipes.map((recipe, si) => {
      if (i !== si) return recipe;
      return { ...recipe, name: evt.target.value };
    });
    this.setState({ recipes: newRecipes });
  }

  handleQuantityChange = (i) => (evt) => {
    const newRecipes = this.state.recipes.map((recipe, si) => {
      if (i !== si) return recipe;
      return { ...recipe, quantity: evt.target.value };
    });

    this.setState({ recipes: newRecipes });
  }

  handleAddStep = () => {
    this.setState({
      steps: this.state.steps.concat([{ description: '' }])
    });
  }

  handleRemoveStep = (i) => () => {
    this.setState({
      steps: this.state.steps.filter((s, si) => i !== si)
    });
  }

  handleStepChange = (i) => (evt) => {
    const newSteps = this.state.steps.map((step, si) => {
      if (i !== si) return step;
      return { ...step, description: evt.target.value };
    });
    this.setState({ steps: newSteps });
  }

  uploadImage(picture) {
    this.setState({
      pictures: this.state.pictures.concat(picture),
    });
  }


  handleSelectCuisine = event => {
    this.setState({
      cuisineSelect: event
    })
  }

  handleFormSubmit = (event) => {
    event.preventDefault();
    const name = event.target.elements.name.value;
    const re = this.state.recipes;
    const cuisine = this.state.cuisineSelect;
    const st = this.state.steps;

    var recipe = "";
    console.log(re.length)
    for (var i = 0; i < re.length; i++) {
      recipe += re[i].name + ',' + re[i].quantity + ';';
    }
    console.log(recipe);

    var steps = "";
    for (var j = 0; j < st.length; j++) {
      steps += st[j].description + ';';
    }
    console.log(steps);
    console.log("rename", name);
    console.log("cuisine", cuisine);

    axios({
      method: 'POST',
      url: 'http://127.0.0.1:8000/api/recipes/',
      headers: {
        'authorization': "JWT " + localStorage.token,
      },
      data: {
        name: name,
        description: recipe,
        creator_id: 1,
        cuisine_type: cuisine,
        steps: steps
      }
    });
  }

  render() {
    return (
      <div className="CreateRecipe">
        <Form onSubmit={(event) => this.handleFormSubmit(
          event
        )}>
          <h2>Name of The Recipe</h2>
          <FormControl
            name="name"
            type="text"
          />
          <br />

          <h2>Cuisine</h2>
          <DropdownButton
            title={this.state.cuisineSelect}
            onSelect={this.handleSelectCuisine}
            id='cuisine'
          >
            {this.state.cuisines.map((cuisine, i) => (
              <MenuItem eventKey={cuisine} key={i}>{cuisine}</MenuItem>
            ))}
          </DropdownButton>
          <br />


          <h2>Ingredients</h2>
          {this.state.recipes.map((recipe, i) => (
            <Form key={i} componentClass="fieldset" inline>
              <FormGroup controlId="ingredients">

                <FormControl name="recipe" type="text" placeholder="Recipe" value={recipe.name} onChange={this.handleRecipeChange(i)} />
                <FormControl.Feedback />
              </FormGroup>{' '}
              <FormGroup controlId="formValidationError4">
                <FormControl type="text" placeholder='Quantity' value={recipe.quantity} onChange={this.handleQuantityChange(i)} />
                <FormControl.Feedback />
              </FormGroup>
              <Button bsStyle='danger' onClick={this.handleRemoveRecipe(i)}>-</Button>
            </Form>
          ))}
          <br />
          <Button bsStyle='primary' onClick={this.handleAddRecipe}>Add Shareholder</Button>
          <br /><br />

          <h2>Steps</h2>
          {this.state.steps.map((step, i) => (
            <FormGroup controlId='recipe' key={i}>
              <InputGroup>
                <FormControl
                  type="text" bsSize='large' placeholder={`Step ${i + 1}`} value={step.description} onChange={this.handleStepChange(i)}
                />
                <FormControl.Feedback />
                <InputGroup.Button>
                  <Button bsSize='large' bsStyle='danger' onClick={this.handleRemoveStep(i)}>-</Button>
                </InputGroup.Button>
              </InputGroup>
            </FormGroup>
          ))}
          <Button bsStyle="primary" onClick={this.handleAddStep} >Add Step</Button>

          <h2>Add Photos</h2>
          <div className='file'>
            <input type='file' multiple />

          </div>
          <br />
          <Button bsSize='lg' bsStyle='success' type="submit">Create</Button>

        </Form>
      </div >
    );
  }
}