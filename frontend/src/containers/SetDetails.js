import React, { Component } from "react";
import { FormControl, Form, FormGroup, Button, InputGroup, DropdownButton, MenuItem, Thumbnail, Col, Row } from "react-bootstrap";
//import {ImageUploader} from "react-images-upload"; //was removed, to pass some test, it wasn't used (add it again if needed)
import "./SetDetails.css";
//import axios from 'axios'

export default class SetDetails extends Component {

  constructor(props) {
    super(props);
    this.state = {
      // recipes: [{ name: '', quantity: '' }],
      // steps: [{description: ''}],
      // pictures: [],
      // cuisines: ['Not Specified', 'Chinese', 'Thai', 'Spanish', 'Italian'],
      // cuisineSelect: 'Not Specified'
      name: 'sb',
      cuisine: 'sss',
      description: 'ingredient1: 4, ingredient2: 2, ingredient3: 5',
      steps: 'jlskdfjklskdlflss. //lskdlfwefklwlfsd. //sdlfkskldf.',
      photos: "food1.jpg, food2.jpg, food2.jpeg, food3.jpg",
      cuisines: ['Not Specified', 'Chinese', 'Thai', 'Spanish', 'Italian'],
      ingredients: [],
      seperateSteps: [],
      seperatePhotos: []
    };
    this.handleSeperateDes = this.handleSeperateDes.bind(this);
    this.handleSeperateSteps = this.handleSeperateSteps.bind(this)
  }

  handleSeperateDes = () => {
    let tempDes = this.state.description.split(',');
    let tempIng = [];
    tempDes.forEach((ing, i) => {
      let temp = {}
      temp.name = ing.split(':')[0].trim()
      temp.quantity = ing.split(':')[1].trim()
      tempIng.push(temp)
    })
    this.setState({ ingredients: tempIng });
  }

  handleAddIngredient = () => {
    this.setState({
      ingredients: this.state.ingredients.concat([{ name: '', quantity: '' }])
    });
  }

  handleRemoveIngredient = (i) => () => {
    this.setState({
      ingredients: this.state.ingredients.filter((s, si) => i !== si)
    });
  }

  handleIngredientChange = (i) => (evt) => {
    const newIngredients = this.state.ingredients.map((ingredient, si) => {
      if (i !== si) return ingredient;
      return { ...ingredient, name: evt.target.value };
    });
    this.setState({ ingredients: newIngredients });
  }

  handleQuantityChange = (i) => (evt) => {
    const newIngredients = this.state.ingredients.map((ingredient, si) => {
      if (i !== si) return ingredient;
      return { ...ingredient, quantity: evt.target.value };
    });

    this.setState({ ingredients: newIngredients });
  }


  handleSeperateSteps = () => {
    let tempSteps = this.state.steps.split('//');
    let tempList = []
    tempSteps.forEach((step, i) => {
      let temp = {};
      temp.description = step;
      tempList.push(temp)
    })
    this.setState({ seperateSteps: tempList })
  }

  handleAddStep = () => {
    this.setState({
      seperateSteps: this.state.seperateSteps.concat([{ description: '' }])
    });
  }

  handleRemoveStep = (i) => () => {
    this.setState({
      seperateSteps: this.state.seperateSteps.filter((s, si) => i !== si)
    });
  }

  handleStepChange = (i) => (evt) => {
    const newSteps = this.state.seperateSteps.map((step, si) => {
      if (i !== si) return step;
      return { ...step, description: evt.target.value };
    });
    this.setState({ seperateSteps: newSteps });
    // let tempSteps = this.state.seperateSteps;
    // tempSteps[i] = evt.target.value;
    // this.setState = ({seperateSteps: tempSteps})
  }

  handleSelectCuisine = event => {
    this.setState({
      cuisine: event
    })
  }



  handleNameChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSeperatePhotos = () => {
    let tempPhotos = this.state.photos.split(',');
    let tempList = []
    tempPhotos.forEach((photo, i) => {
      let temp = {};
      temp.name = photo.trim();
      tempList.push(temp)
    })
    this.setState({ seperatePhotos: tempList })
  }

  handleRemovePhoto = (i) => () => {
    this.setState({
      seperatePhotos: this.state.seperatePhotos.filter((s, si) => i !== si)
    });
  }

  handlePhotos = () => {
    let content = []
    let row = []
    this.state.seperatePhotos.forEach((photo, i) => {
      if ((i) % 3 === 0) {
        row = []
        row.push(
          <Col xs={6} md={4} key={i}>
            <Thumbnail src={require("../temp_pic/" + photo.name)} />
            <Button bsStyle="danger" onClick={this.handleRemovePhoto(i)}>Delete</Button>
          </Col>
        )
      }
      else {
        row.push(
          <Col xs={6} md={4} key={i}>
            <Thumbnail src={require("../temp_pic/" + photo.name)} />
            <Button bsStyle="danger" onClick={this.handleRemovePhoto(i)}>Delete</Button>
          </Col>
        )
      }
      if (i % 3 === 2 || (i + 1) === this.state.seperatePhotos.length) {
        content.push(<Row key={i}>{row}</Row>)
      }
    })
    return (content)
  }

  componentWillMount = () => {
    this.handleSeperateDes();
    this.handleSeperateSteps();
    this.handleSeperatePhotos()
  }

  handleJoinIngredients = () => {

  }

  handleJoinSteps = () => {

  }

  handleJoinPhotos = () => {

  }

  handleDelete = async event => {

  }

  handleFormSubmit = (event) => {
    // event.preventDefault();
    // const name = event.target.elements.name.value;
    // const re = this.state.recipes;
    // const cuisine = event.target.elements.cuisine.value;
    // const st = this.state.steps;

    // var recipe = "";
    // console.log(re.length)
    // for(var i=0;i<re.length;i++){
    //   recipe += re[i].name+','+re[i].quantity+';';
    // }
    // console.log(recipe);

    // var steps = "";
    // for(var i=0; i<st.length;i++){
    //     steps += st[i].description+';';
    // }
    // console.log(steps);

    // console.log("rename",name);
    // console.log("cuisine",cuisine);

  }


  render() {

    return (
      <div className="SetRecipe">
        <Form onSubmit={(event) => this.handleFormSubmit(
          event
        )}>
          <h2>Name of The Recipe</h2>
          <FormControl
            id="name"
            type="text"
            value={this.state.name}
            onChange={this.handleNameChange}
          />
          <br />

          <h2>Cuisine</h2>
          <DropdownButton
            title={this.state.cuisine}
            onSelect={this.handleSelectCuisine}
            id="cuisine"
          >
            {this.state.cuisines.map((cuisine, i) => (
              <MenuItem eventKey={cuisine} key={i}>{cuisine}</MenuItem>
            ))}
          </DropdownButton>
          <br />


          <h2>Ingredients</h2>

          {this.state.ingredients.map((ingredient, i) => (
            <Form key={i} componentClass="fieldset" inline>
              <FormGroup controlId="ingredients">

                <FormControl name="recipe" type="text" placeholder="Recipe" value={ingredient.name} onChange={this.handleIngredientChange(i)} />
                <FormControl.Feedback />
              </FormGroup>{' '}
              <FormGroup controlId="formValidationError4">
                <FormControl type="text" placeholder='Quantity' value={ingredient.quantity} onChange={this.handleQuantityChange(i)} />
                <FormControl.Feedback />
              </FormGroup>
              <Button bsStyle='danger' onClick={this.handleRemoveIngredient(i)}>-</Button>
            </Form>
          ))}
          <br />
          <Button bsStyle='primary' onClick={this.handleAddIngredient}>Add Shareholder</Button>
          <br /><br />

          <h2>Steps</h2>
          {this.state.seperateSteps.map((step, i) => (
            <FormGroup controlId='recipe' key={i}>
              <InputGroup>
                <FormControl
                  type="text" bsSize='large' placeholder={`Step ${i + 1}`} value={step.description} onChange={this.handleStepChange(i)}
                />
                <FormControl.Feedback />
                <InputGroup.Button>
                  <Button bsSize='large' bsStyle='danger' onClick={this.handleRemoveStep(i)}>-</Button>
                </InputGroup.Button>
              </InputGroup>
            </FormGroup>
          ))}
          <Button bsStyle="primary" onClick={this.handleAddStep} >Add Step</Button>

          <br /><br />
          <h2>Photos</h2>
          <div className='file'>
            <input type='file' multiple />
            {this.handlePhotos()}
          </div>
          <br /><br /><br />
          <div className="well" style={{ maxWidth: 400, margin: '0 auto 10px' }}>
            <Button bsStyle="success" bsSize="large" block type="submit">
              Save
          </Button>
            <Button bsStyle='danger' bsSize="large" block onClick={this.handleDelete}>
              Delete
          </Button>
          </div>


        </Form>
      </div>
    );
  }
}