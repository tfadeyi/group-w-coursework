import React, { Component } from "react";
import { FormGroup, FormControl, ControlLabel, Button } from "react-bootstrap";
import axios from 'axios';
import "./Signup.css"
export default class Signup extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: "",
            usernaem: '',
            password: "",

        };
    }

    validateForm() {
        return this.state.email.length > 0 && this.state.password.length > 0 && this.state.username.length > 0;
    }


    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleSubmit = async event => {
        event.preventDefault();
        axios.post('http://127.0.0.1:8000/api/users/', {
            email: this.state.email,
            username: this.state.username,
            password: this.state.password
        });
        this.props.history.push("/login")
    }

    render() {
        return (
            <div className="signup">
                <form onSubmit={this.handleSubmit}>
                    <center><ControlLabel><h1>Sign up an account</h1></ControlLabel></center>
                    <FormGroup controlId="email" bsSize="large">
                        <ControlLabel>Email</ControlLabel>
                        <FormControl
                            autoFocus
                            type="email"
                            value={this.state.email}
                            onChange={this.handleChange}
                        />
                    </FormGroup>

                    <FormGroup controlId="username" bsSize="large">
                        <ControlLabel>username</ControlLabel>
                        <FormControl
                            value={this.state.username}
                            onChange={this.handleChange}
                            type="username"
                        />
                    </FormGroup>

                    <FormGroup controlId="password" bsSize="large">
                        <ControlLabel>Password</ControlLabel>
                        <FormControl
                            value={this.state.password}
                            onChange={this.handleChange}
                            type="password"
                        />
                    </FormGroup>
                    <center>
                        <Button type="submit" disabled={!this.validateForm()}>
                            Signup
                    </Button>
                    </center>
                </form>
            </div>
        );
    }
}