import React, { Component } from "react";
import { FormGroup, FormControl, ControlLabel, Button } from "react-bootstrap";
import axios from 'axios';
import "./Login.css"

export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            username: '',
            password: "",
            info: '',
            userid: '',
        };
    }

    validateForm() {
        return this.state.email.length > 0 && this.state.password.length > 0 && this.state.username.length > 0;
    }


    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleSubmit = async event => {
        event.preventDefault();
        // console.log(this.state.email, this.state.username, this.state.password)
        try {
            await axios.post('http://127.0.0.1:8000/api/auth/login/', {
                email: this.state.email,
                username: this.state.username,
                password: this.state.password
            })
                .then(data => (this.setState({
                    info: data.data.token,
                    username: data.data.user.username,
                    userid: data.data.user.pk,
                    email: data.data.user.email,
                })))
            this.props.userHasAuthenticated(true);
            // add this to test the log in function.
        } catch (e) {
            console.log(e)
        }
        localStorage['username'] = this.state.username
        localStorage['token'] = this.state.info
        localStorage['userid'] = this.state.userid
        localStorage['email'] = this.state.email

        console.log(localStorage)
        this.props.history.push("/user")
        // store the token, username and userid into the localstorage
    }

    render() {
        return (
            <div className="login">
                <form onSubmit={this.handleSubmit}>
                    <center><ControlLabel><h1>Login</h1></ControlLabel></center>
                    <FormGroup controlId="email" bsSize="large">
                        <ControlLabel>Email</ControlLabel>
                        <FormControl
                            autoFocus
                            type="email"
                            value={this.state.email}
                            onChange={this.handleChange}
                        />
                    </FormGroup>

                    <FormGroup controlId="username" bsSize="large">
                        <ControlLabel>username</ControlLabel>
                        <FormControl
                            value={this.state.username}
                            onChange={this.handleChange}
                            type="username"
                        />
                    </FormGroup>

                    <FormGroup controlId="password" bsSize="large">
                        <ControlLabel>Password</ControlLabel>
                        <FormControl
                            value={this.state.password}
                            onChange={this.handleChange}
                            type="password"
                        />
                    </FormGroup>
                    <center>
                        <Button type="submit" disabled={!this.validateForm()}>
                            Login
                            </Button>
                    </center>
                </form>
            </div>
        );
    }
}