
import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "./containers/Home";
import UserHome from "./containers/UserHome";
import CreateRecipe from "./containers/CreateRecipe"
import Login from "./containers/Login"
import Signup from "./containers/Signup"
import AdvancedSearch from "./containers/AdvancedSearch"
import ShowDetails from "./containers/ShowDetails"
import SetDetails from "./containers/SetDetails"
import AuthenticatedRoute from "./components/AuthenticatedRoute";
import UnauthenticatedRoute from "./components/UnauthenticatedRoute";

export default ({ childProps }) =>
  <Switch>
    <Route path="/" exact component={Home} />
    <UnauthenticatedRoute path="/login" exact component={Login} props={childProps} />
    <UnauthenticatedRoute path="/Signup" exact component={Signup} props={childProps} />
    <AuthenticatedRoute path="/user" exact component={UserHome} props={childProps} />
    <AuthenticatedRoute path="/user/create" exact component={CreateRecipe} props={childProps} />
    <Route path="/advsearch/:search" exact component={AdvancedSearch} props={childProps} />
    <AuthenticatedRoute path="/user/recipeId" exact component={ShowDetails} props={childProps} />
    <AuthenticatedRoute path="/user/myrecipeId" exact component={SetDetails} props={childProps} />
    <AuthenticatedRoute path="/user/myrecipeId" exact component={SetDetails} props={childProps} />


  </Switch>;